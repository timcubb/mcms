<?php

  include('lib/fitzgerald.php');

class AdminApplication extends Fitzgerald {

	// Redefine the constructor to setup the app
    
    public function __construct($options=array()) {
        session_set_cookie_params(3600);
        parent::__construct($options);
    }


    /* **************************************
     *
     *      LOGIN STUFF
     *
     ************************************** */



	public function get_logout() {
        $this->logout();
        return $this->redirect('/login');
    }

    public function post_login() {
        if ($this->login($this->request->username, $this->request->password)) {
            return $this->redirect('/secret');
        } else {
            $this->error = 'Invalid username or password';
            return $this->redirect('/login');
        }
    }

    public function get_secret($page) {
        return $this->render($page, array('data' => 'my test data'));
    }

    // before filters

    protected function verify_user() {
        if (is_null($this->session->user) || !$this->isValidUser($this->session->user)) {
            return $this->redirect('/login');
        }
    }

    // Helper methods

    private function isLoggedIn() {
        if (!is_null($this->session->user) && $this->isValidUser($this->session->user)) {
            return true;
        } else {
            $this->logout();
            return false;
        }
    }

    private function isValidUser($username) {
        return $username == 'tim';
    }

    private function login($username, $password) {
        return $username == 'tim' && $password == 'mit123';
    }

    private function logout() {
        $this->session->user = null;
        session_destroy();
    }



    /* **************************************
     *
     *      START APP STUFF
     *
     ************************************** */



    public function get_index() {

    	$var1 = 'Value to use in template';
        $var2 = 'Another value to use in template';

        return $this->render('index', compact('var1', 'var2'));
    }


    public function get_pages() {
        return $this->render('pages/index');
    }

    public function get_page($page_id) {
        return $this->render('pages/view_page', compact('page_id'));
    }


    public function get_update_page($page_id) {
        return $this->render('pages/update_page', compact('page_id'));
    }

    public function post_update_page($page_id) {
        return $this->render('pages/update_page', compact('page_id'));
    }


    public function get_new_page($page_id) {
        return $this->render('pages/new_page', compact('page_id'));
    }

    public function post_new_page($page_id) {
        return $this->render('pages/new_page', compact('page_id'));
    }















}
$app = new AdminApplication(array('layout' => 'admin_template'));

$app->before('get_secret|another_action', 'verify_user');

// langing page (post login)
$app->get('/', 'get_index');

// PAGES

// list all pages
$app->get('/pages', 'get_pages');

// view a selected page
$app->get('/page/:id', 'get_page');

// Update page
$app->get('/page/update/:id', 'get_update_page');
$app->post('/page/update/:id', 'post_update_page');

// new page
$app->get('/page/new/:id', 'get_new_page');
$app->post('/page/new/:id', 'post_new_page');


// WIDGETS

// list all widgets
$app->get('/widgets', 'get_widgets');

// view a selected widget
$app->get('/widget/:id', 'get_widget');

// Update widget
$app->get('/widget/update/:id', 'get_update_widget');
$app->post('/widget/update/:id', 'post_update_widget');

// new widget
$app->get('/widget/new/:id', 'get_new_widget');
$app->post('/widget/new/:id', 'post_new_widget');


$app->run();

?>