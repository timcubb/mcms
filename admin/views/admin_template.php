<!doctype tml>
<html>
<head>
	<title></title>
</head>
<body>
	<div id="container">
		<div id="header"></div>
		<div id="navigation">
			<ul>
				<li>item</li>
				<li>item</li>
				<li>item</li>
				<li>item</li>
				<li>item</li>
				<li>item</li>
			</ul>
		</div>
		<div id="content_columns">
			<div id="column1">
				<!-- Left side menu -->
				<h2>Left Navigation</h2>
				<ul>
					<li>item</li>
					<li>item</li>
					<li>item</li>
					<li>item</li>
					<li>item</li>
					<li>item</li>
				</ul>
			</div>
			<div id="column2">
				Content area
				<? echo $content; ?>
				<!-- Content area -->
			</div>
		</div>
		<div id="footer">&copy 2013 MCMS</div>
	</div>
</body>
</html