<?php

/* =============================

    This is just a sample base application (user site ("/")) 
    "/admin" is the actual CMS files. So this is a placeholder
    of where your site/app would live, and then yoursite.com/admin
    will bring up the cms admin where you can control the site's CMS.

===============================*/

include('lib/fitzgerald.php');

class BaseApplication extends Fitzgerald {

    public function get_index() {

        return $this->render('index');
    }

}

$app = new BaseApplication();

$app->get('/', 'get_index');
$app->run();

?>